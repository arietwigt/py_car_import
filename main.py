from car_functions.cardata import CarData, CarDataCollection

if __name__ == "__main__":
    # initiate the CarDataCollection class
    my_car_collection = CarDataCollection('My collection')

    # initiate a new instance of CarData
    my_car_data = CarData("kia", "rood")
    my_car_data_2 = CarData("peugeot", "rood")
    my_car_data_3 = CarData("citroen", "rood")
    my_car_data_4 = CarData("ferrari", "rood")
    my_car_data_5 = CarData("arie", "rood")
    my_car_data_6 = CarData("ford", "rood")
    my_car_data_7 = CarData("jaguar", "rood")
    my_car_data_8 = CarData("volvo", "rood")

    # add a CarData to the CarDataCollection
    my_car_collection.add_car_data_to_collection(my_car_data,
                                                 my_car_data_2,
                                                 my_car_data_3,
                                                 my_car_data_4,
                                                 my_car_data_5)

    # import the data for all the cars
    my_car_collection.import_data_all_cars()

    # convert all cars
    my_car_collection.convert_data_all_cars()

    # export the data of all cars in seperate data folders and datasets
    my_car_collection.export_data_all_cars()

    # expor the data of all cars in a combined dataset
    my_car_collection.export_combined_dataset()
