from car_functions.cardata import CarData


# tests to check if the right brand is imported
def test_right_brand():
    # initiate an instance for CarData
    my_car_data = CarData("audi", "rood")

    # check for the right brand
    assert my_car_data.brand == "audi"


def test_right_brand_import():
    # initiate an instance for CarData
    my_car_data = CarData("audi", "rood")
    my_car_data.import_cars_brand()
    
    # from the cars list, check the first value
    assert my_car_data.cars_list[0]['merk'] == 'AUDI'
    assert my_car_data.cars_list[-1]['merk'] == 'AUDI'


def test_right_color_import():
    # initiate an instance for CarData
    my_car_data = CarData("audi", "rood")
    my_car_data.import_cars_brand()
    
    # test for the colour red
    assert my_car_data.cars_list[0]['eerste_kleur'] == "ROOD"
    assert my_car_data.cars_list[-1]['eerste_kleur'] == "ROOD"

    # initiate an instance for CarData
    my_car_data = CarData("fiat", "zwart")
    my_car_data.import_cars_brand()
    
    # test for the colour red
    assert my_car_data.cars_list[0]['eerste_kleur'] == "ZWART"
    assert my_car_data.cars_list[-1]['eerste_kleur'] == "ZWART"
    

if __name__ == "__main__":
    test_right_color_import()
    pass
