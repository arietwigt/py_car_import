import requests
from car_functions import ENDPOINT_URL, DEFAULT_COLNAMES, DEFAULT_COL_RENAMES, EXPORT_FOLDER_NAME
import pandas as pd
import os
import uuid


class CarDataCollection():
    
    # class attribrute
    continue_if_failed = True

    def __init__(self, name: str):
        '''
        Initialize a new CarDataCollection
        '''
        self.name = name
        self.car_data_list = []

    # method for adding a CarData class
    def add_car_data_to_collection(self, *args) -> None:
        '''
        Add a CarData Instance to the collection
        '''

        # for earch added car, add it to the car_data list
        for car_data in args:
            print(f"Adding car: {car_data.brand}")
            # if type(car_data) != CarData:
            # raise TypeError(f"Wrong type. Type is {type(car_data)}. Should be CarData")
            # else:
            self.car_data_list.append(car_data)

    # method for showing the cars in the collection
    def show_cars_in_collection(self) -> None:
        '''
        Method for showing all cars in the current collection
        '''
        for car in self.car_data_list:
            print(f"{car.brand}")

    # execute the import of cars for all the cars
    def import_data_all_cars(self):
        '''
        Call all CarData objects to import the data from the API
        '''
        ids_to_delete = []

        for idx, car_data in enumerate(self.car_data_list):
            if self.continue_if_failed:
                try:
                    car_data.import_cars_brand()
                except ValueError:
                    print(f"No cars found for {car_data.brand}")

                    # delete the car from the list
                    ids_to_delete.append(idx)
                    continue
            else:
                car_data.import_cars_brand()

        # delete the index of the list with the car that could not be imported
        for id_to_delete in ids_to_delete:
            self.car_data_list.pop(id_to_delete)
        pass

    # execute the import of cars for all the cars
    def convert_data_all_cars(self):
        '''
        Call all the CarData objects to apply the conversion steps for the Pandas DataFrames
        '''
        for car_data in self.car_data_list:
            car_data.convert_list_to_df()

    # execute the export of cars dataframe to csv
    def export_data_all_cars(self):
        '''
        Call all the CarData objects to export the data to csv
        '''
        for car_data in self.car_data_list:
            car_data.export_df_to_csv()

    # def method for creating and exporting the file with all the imported data combined
    def export_combined_dataset(self):
        '''
        Method for exporting the Pandas DataFrames combined
        '''
        # get the cars_list from the instance
        car_data_list = self.car_data_list

        # initiate an empty list to collect the DataFrames
        car_data_frames_list = []

        # iterate over the car_data instances in the car_data_list to get the DataFrames
        for car_data in car_data_list:
            car_data_frames_list.append(car_data.cars_df)

        # combine all the panda's DataFrames
        cars_df_combined = pd.concat(car_data_frames_list)

        # store the data to the attribute
        self.cars_df_combined = cars_df_combined

        # export the combined pandas DataFrame
        self.export_df_to_csv(combined_df=True, folder_name="combined_data")

    def export_df_to_csv(self, combined_df=False, folder_name=None) -> None:
        """
        Function to export the pandas DataFrame to csv

        """
        if not folder_name:
            folder_name = self.brand

        # define the export path
        export_folder_path = f"{EXPORT_FOLDER_NAME}/{folder_name}"

        # create the folder
        os.makedirs(export_folder_path, exist_ok=True)

        # generate the unique identifier for the export file
        file_uuid = uuid.uuid4()

        # define the export file path
        export_file_path = f"{export_folder_path}/export_{folder_name}_{file_uuid}.csv"

        # if it is a combined set, get it from the combined_df attribute
        if combined_df:
            self.cars_df_combined.to_csv(export_file_path, sep=";", index=False)
            print("🗂️  Succesfully exported dataset for the collection of brands")
        else:
            # use pandas to export the data frame
            self.cars_df.to_csv(export_file_path, sep=";", index=False)
            # update the status and steps
            self.steps.append('exported')
            self.status = 'Exported'

            print("🚘 Succesfully exported dataset for the brand")

    # define the representation
    def __repr__(self):
        number_of_cars = len(self.car_data_list)
        return f"{self.name} - Number of cars:{number_of_cars}"


class CarData(CarDataCollection):

    def __init__(self,
                 brand: str,
                 color: str = None) -> None:
        '''
        Initialialize a CarData instance

        Arguments:
        * brand: Brand of the car
        * color: Color of the car
        '''

        self.brand = brand
        self.color = color
        self.steps = []
        self.status = "Initialized"
        self.cars_list = None
        self.cars_df = pd.DataFrame()

    # method for importing the car data based on the brand
    def import_cars_brand(self) -> None:
        """
        Returns a list of dictionaries with cars imported from the RDW

        """

        # Get the brand attribute from the instance
        brand = self.brand

        # uppercase the brand variable
        brand_upper = brand.upper()

        # define the endpoint
        endpoint = f"{ENDPOINT_URL}?merk={brand_upper}"

        # if the color is not None
        if self.color:
            color_upper = self.color.upper()
            endpoint += f"&eerste_kleur={color_upper}"

        # execute the request for the endpoint
        print(f"Importing data for {self.brand}")
        response = requests.get(endpoint)

        # get the data from the response
        data = response.json()

        # check if the list is empty
        if len(data) == 0:
            raise ValueError(f"No cars found for :'{brand} - {self.color}'")
 
        # store the data in the instance
        self.cars_list = data

        # update the steps and the status
        self.steps.append('Imported')
        self.status = "Imported"


    # Method for converting the cars list to a DataFrame
    def convert_list_to_df(self,
                           *args,
                           replace_colnames=False,
                           replace_col_renames=False,
                           **kwargs) -> pd.DataFrame:
        """
        Converts a list to a pandas DataFrame
        """

        # get the cars_list attribute from the class instance
        cars_list = self.cars_list

        # create the DataFrame from scratch
        df = pd.DataFrame(cars_list)

        # get the possible additional column names
        additional_colnames = []

        for colname in args:
            if colname in df.columns:
                additional_colnames.append(colname)
                print(f"🧹 Column {colname} not in the data frame. Removing")
        
        # specify to add or replace the column names
        colnames_df = []

        if replace_colnames:
            colnames_df += additional_colnames
        else:
            colnames_df += DEFAULT_COLNAMES + additional_colnames

        # specify the columns
        df_subset = df[colnames_df]

        # for each keyword argument (kwarg), check if the column exists
        for colname in kwargs.keys():
            if colname not in df_subset.columns:
                raise KeyError(f"The column {colname} not in the DataFrame")

        # get the column names from kwargs
        colname_replaces = {}

        if replace_col_renames: # if we only to use the keyword arguments for replacing the column names
            colname_replaces.update(kwargs)
        else: # if we want both the keyword arguments and default colname replacements
            colname_replaces.update(kwargs) # use the keyword arguments
            colname_replaces.update(DEFAULT_COL_RENAMES) # and, use the default renames

        # possibility to rename the columns
        df_subset.rename(columns=colname_replaces, inplace=True)

        # store the data in the class instance
        self.cars_df = df_subset

        # update the status and steps
        self.steps.append('Converted')
        self.status = 'Converted'
        

    def __repr__(self) -> None:
        return f"Car import of Brand: {self.brand} - Color: {self.color}" + \
               f"Status: {self.status} steps: {self.steps}" + \
               f"Cars data: {self.cars_df.head(3)}"
