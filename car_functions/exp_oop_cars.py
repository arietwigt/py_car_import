from typing import Union


class Vehicle:
    # specify the initial attributes of a Car
    # a constructor
    def __init__(self, brand: str,
                       model: str,
                       color: str,
                       price: float=0) -> None:
        self.brand = brand
        self.model = model
        self.color = color
        self.price = price


    # method to change the price of the car
    def change_price(self, price_change: Union[float, int]) -> None:
        '''
        Change the price of a car
        '''

        # define the current price of the car
        old_price = self.price

        if old_price + price_change < 0:
            raise ValueError("Changing this price will make the value lower than zero")

        # change the price
        self.price += price_change

        # print the price change
        print(f"Changed price from {old_price} to {self.price}")


    # define how a instance of the class is represented
    def __repr__(self) -> str:
        return f"{self.brand} - {self.model} - {self.color} - {self.price}"


class MotorCycle(Vehicle):
    # class attribute
    wheels = 2

    # method
    def my_function(self):
        print("From the motor")

    pass


class Car(Vehicle):
    # class attribute
    wheels = 4

    # method
    def my_function(self):
        print("From the car")

    pass



'''

Vehicle (parent)
--Car (child)
--MotorCycle (child)
'''
