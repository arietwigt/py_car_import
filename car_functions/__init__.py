import os

EXPORT_FOLDER_NAME = "export"

# check if the export folder is present
if not os.path.exists(EXPORT_FOLDER_NAME):
    print("🗂️ Creating export folder")
    os.mkdir(EXPORT_FOLDER_NAME)


# endpoint for the RDW
ENDPOINT_URL = "https://opendata.rdw.nl/resource/m9d7-ebf2.json"

# define the default column names
DEFAULT_COLNAMES = ['merk', 'handelsbenaming', \
                    'eerste_kleur', 'catalogusprijs',
                    'aantal_cilinders']


# define the default column renames
DEFAULT_COL_RENAMES = {"catalogusprijs": "price",
                       "eerste_kleur": "color"}
