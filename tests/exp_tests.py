import pytest
from typing import Union


# method to calculate the interest of a amount of money in a certain term
def calc_interest(amount: Union[int, float],
                  interest_rate: Union[int, float],
                  years: int,
                  round_value: int=3) -> float:
    '''
    Method for calculating the interest for an amount of money
    for a certain interest in a period of time in years
    '''

    # Validate types
 
    amount_intrest_types = (int, float)
    years_datatype = int
 
    if type(amount) not in amount_intrest_types:
        raise TypeError(f"Data type of {type(amount)} is not correct")
   
    if type(interest_rate) not in amount_intrest_types:
        raise TypeError(f"Data type of {type(interest_rate)} is not correct")
 
    if type(years) != years_datatype:
        raise TypeError(f"Data type of {type(years)} is not correct")


    # check if the value is positive
    if amount < 0:
        raise ValueError(f"Value should not be lower than zero. Got {amount}")

    # check if the years are not too large
    if not 0 < years < 100:
        raise ValueError(f"Value for years should be between 0 and 100. Got {years}")

    # apply the calculation
    final_amount = amount * (1 + interest_rate) ** years

    # round the value
    final_amount_rounded = round(final_amount, round_value)
    
    return final_amount_rounded


# define the tests
def test_calc_interest():
    assert calc_interest(100, 0.1, 1) == 110
    assert calc_interest(100, 0.2, 1) == 120


def test_calc_interest_wrong_values():
    # test if the amount is negative
    with pytest.raises(ValueError):
        assert calc_interest(-100, 0.1, 1)
    
    with pytest.raises(ValueError):
        assert calc_interest(100, 0.1, 10000)

    with pytest.raises(ValueError):
        assert calc_interest(100, 0.1, -10)


def test_calc_interest_wrong_datatype():
    # test if the data type is wrong
    with pytest.raises(TypeError):
        assert calc_interest(100, "0.1", 1)
 
    with pytest.raises(TypeError):
        assert calc_interest(100, "0.1", "s")
 
 
    with pytest.raises(TypeError):
        assert calc_interest(" ", "0.1", "s")
