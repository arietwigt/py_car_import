# Python car app


## Installation


## Install `virtualenv`

`pip3 install virtualenv`


## Activate the virtual environment

UNIX:
`. venv/bin/activate`

WINDOWS

`venv/Scripts/activate`


## Install the modules

`pip install -r requirements.txt`


## Run the application


Command line:

`python main.py`


Debugger:

- Press play
