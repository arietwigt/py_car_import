import redis
import pandas as pd
import json

# Create a redis client
redisClient = redis.StrictRedis(host='localhost', port=6379, db=0)

# Create a dataframe
dd = {'ID': ['H576','H577','H578','H600', 'H700'],
              'CD': ['AAAAAAA', 'BBBBB', 'CCCCCC','DDDDDD', 'EEEEEEE']}

# convert the dictionary to a data frame
df = pd.DataFrame(dd)

# convert to json
data = df.to_json()

# set the redis client
redisClient.set('data', data)

# Retrieve the data from redis
blob = redisClient.get('data')

# load the data (as string) to a python dictionary
blob_dict = json.loads(blob)

# read the data from the json blob
df_from_redis = pd.DataFrame(blob_dict)

# show the data
df_from_redis.head()
